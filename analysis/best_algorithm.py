from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np


def process_file(l, k):
    file_name = "results_mac_x86/" + str(l) + "_" + str(k) + ".csv"

    f = open(file_name)
    mat = np.loadtxt(open(file_name, "r"), delimiter=";").T

    mat = [mat[0], mat[2], mat[3], mat[5], mat[6], mat[8], mat[9]]

    print(len(mat))




    return list(map(np.min, list(mat)))


lambda_range = range(2, 14)
k_range = range(2, 14)

matrix = [[0 for l in lambda_range] for k in k_range]

for l in lambda_range:
    for k in lambda_range:
        values = process_file(2**l,2**k)
        matrix[k-k_range[0]][l-lambda_range[0]] = values.index(min(values))+1


for l in matrix[::-1]:
    print(l)


cmap = colors.ListedColormap(['red',
                              'limegreen', 'lightgreen',
                              'dodgerblue', 'lightskyblue',
                              'orchid', 'darkorchid'])
bounds = [1, 2, 3, 4, 5, 6, 7, 8]
norm = colors.BoundaryNorm(bounds, cmap.N)

fig, ax = plt.subplots()
img = ax.imshow(np.array(matrix).T, cmap=cmap, norm=norm, origin="lower")
cbar = plt.colorbar(img, ticks=[1, 2, 3, 4, 5, 6, 7])
cbar.set_ticks([])

x = 15

cbar.ax.text(x, 1.40, "Separate")
cbar.ax.text(x, 2.40, "2-$2^w$-ary")
cbar.ax.text(x, 3.40, "2-YLL")
cbar.ax.text(x, 4.40, "3-$2^w$-ary")
cbar.ax.text(x, 5.40, "3-YLL")
cbar.ax.text(x, 6.40, "4-$2^w$-ary")
cbar.ax.text(x, 7.40, "4-YLL")



ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=1)

print(np.arange(-0.5, k_range[-1] - k_range[0], 1))
print(k_range[-1] - k_range[0])

plt.xticks(np.arange(-0.5, k_range[-1] - k_range[0]+1, 1))
ax.set_xticklabels('')
ax.set_xticks(np.arange(0, k_range[-1] - k_range[0]+1, 1), minor=True)
ax.set_xticklabels(["$2^{" + str(k) + "}$" for k in k_range], minor=True)


plt.yticks(np.arange(-0.5, lambda_range[-1] - lambda_range[0]+1, 1))
ax.set_yticklabels('')
ax.set_yticks(np.arange(0, lambda_range[-1] - lambda_range[0]+1, 1), minor=True)
ax.set_yticklabels(["$2^{" + str(l) + "}$" for l in lambda_range], minor=True)

ax.tick_params(axis='both', which='both', length=0)


plt.xlabel("Value of $k$")
plt.ylabel("Value of $\lambda$")

plt.savefig("best_algo_mac_x86.pdf")
