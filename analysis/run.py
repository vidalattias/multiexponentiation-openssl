import os

k_values = [2**k for k in range(2, 13)]
lambda_values = [2**k for k in range(2, 13)]
repeat = 10
large_repeat = 10

for i in range(large_repeat):
    print(i)
    for k in k_values:
        print("\t" + str(k))
        for l in lambda_values:
            print("\t\t" + str(l))
            cmd = 'src/multiexp ' + \
                str(l) + ' ' + str(k) + ' ' + str(repeat)

            output_file = "results/" + str(l) + "_" + str(k) + ".csv"
            os.system(cmd + " >> " + output_file)
