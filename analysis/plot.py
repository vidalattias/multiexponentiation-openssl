import matplotlib.pyplot as plt
import math
import numpy as np


lmin = 7
lmax = 13
kmin = 7
kmax = 13

x = range(2, 14)

xx = [2**x for x in x]


def process_file(l, k):
    file_name = "results/" + str(l) + "_" + str(k) + ".csv"

    f = open(file_name)
    mat = np.loadtxt(open(file_name, "r"), delimiter=";")
    return list(map(np.mean, list(mat.T)))


for l in x:
    naive = []
    lenstra = []
    lenstra_montgomery = []
    w_arry = []

    for k in x:
        data = process_file(2**l, 2**k)

        naive.append(data[0])
        lenstra.append(data[1])
        lenstra_montgomery.append(data[2])
        w_arry.append(data[3])

    plt.plot(xx, naive, label="Naive")
    plt.plot(xx, lenstra, label="Lenstra")
    plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
    plt.plot(xx, w_arry, label="w-arry")
    plt.xscale('log')
    plt.legend()

    plt.savefig('plots/k_axis/w=2/'+str(2**l)+'.pdf')
    plt.cla()


for l in x:
    naive = []
    lenstra = []
    lenstra_montgomery = []
    w_arry = []

    for k in x:
        data = process_file(2**l, 2**k)

        naive.append(data[0])
        lenstra.append(data[4])
        lenstra_montgomery.append(data[5])
        w_arry.append(data[6])

    plt.plot(xx, naive, label="Naive")
    plt.plot(xx, lenstra, label="Lenstra")
    plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
    plt.plot(xx, w_arry, label="w-arry")
    plt.xscale('log')
    plt.legend()

    plt.savefig('plots/k_axis/w=3/'+str(2**l)+'.pdf')
    plt.cla()


for l in x:
    naive = []
    lenstra = []
    lenstra_montgomery = []
    w_arry = []

    for k in x:
        data = process_file(2**l, 2**k)

        naive.append(data[0])
        lenstra.append(data[7])
        lenstra_montgomery.append(data[8])
        w_arry.append(data[9])

    plt.plot(xx, naive, label="Naive")
    plt.plot(xx, lenstra, label="Lenstra")
    plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
    plt.plot(xx, w_arry, label="w-arry")
    plt.xscale('log')
    plt.legend()

    plt.savefig('plots/k_axis/w=4/'+str(2**l)+'.pdf')
    plt.cla()









    ### L AXIS
    for k in x:
        naive = []
        lenstra = []
        lenstra_montgomery = []
        w_arry = []

        for l in x:
            data = process_file(2**l, 2**k)

            naive.append(data[0])
            lenstra.append(data[1])
            lenstra_montgomery.append(data[2])
            w_arry.append(data[3])

        plt.plot(xx, naive, label="Naive")
        plt.plot(xx, lenstra, label="Lenstra")
        plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
        plt.plot(xx, w_arry, label="w-arry")

        plt.legend()

        plt.xscale('log')

        plt.savefig('plots/l_axis/w=2/'+str(2**k)+'.pdf')
        plt.cla()


    for k in x:
        naive = []
        lenstra = []
        lenstra_montgomery = []
        w_arry = []

        for l in x:
            data = process_file(2**l, 2**k)

            naive.append(data[0])
            lenstra.append(data[4])
            lenstra_montgomery.append(data[5])
            w_arry.append(data[6])

        plt.plot(xx, naive, label="Naive")
        plt.plot(xx, lenstra, label="Lenstra")
        plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
        plt.plot(xx, w_arry, label="w-arry")
        plt.xscale('log')
        plt.yscale('log')
        plt.legend()

        plt.savefig('plots/l_axis/w=3/'+str(2**l)+'.pdf')
        plt.cla()


    for k in x:
        naive = []
        lenstra = []
        lenstra_montgomery = []
        w_arry = []

        for l in x:
            data = process_file(2**l, 2**k)

            naive.append(data[0])
            lenstra.append(data[7])
            lenstra_montgomery.append(data[8])
            w_arry.append(data[9])

        plt.plot(xx, naive, label="Naive")
        plt.plot(xx, lenstra, label="Lenstra")
        plt.plot(xx, lenstra_montgomery, label="Lenstra Montgomery")
        plt.plot(xx, w_arry, label="w-arry")
        plt.xscale('log')
        plt.legend()

        plt.savefig('plots/l_axis/w=4/'+str(2**l)+'.pdf')
        plt.cla()
