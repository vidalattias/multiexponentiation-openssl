import matplotlib.pyplot as plt


prio = []
no = []
normal = []

file = open('tmp/prio.csv')
for line in file:
    prio.append(1000 * float(line.split(';')[0]))

file = open('tmp/normal.csv')
for line in file:
    normal.append(1000 * float(line.split(';')[0]))

file = open('tmp/no.csv')
for line in file:
    no.append(1000 * float(line.split(';')[0]))

plt.boxplot([prio, normal, no], sym='')

plt.xticks([1, 2, 3], ["Max priority", 'Normal priority', 'Minimal priority'])
plt.ylabel("Computation time (ms)")

plt.grid(which='both')
plt.savefig("priority.pdf")
plt.show()
