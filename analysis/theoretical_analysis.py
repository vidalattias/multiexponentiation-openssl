import math
import matplotlib.pyplot as plt

import argparse
import sys

import matplotlib.pyplot as plt
from cycler import cycler
import numpy as np

def init_plot(figsize=None):
    f = plt.figure()

    font = {
        'weight': 'normal',
        'size': 16,
    }
    plt.rc('font', **font)

    # prepare plots with markers so that they're readable in black and white
    monochrome = (
        cycler('marker', ['^', 'v', '<', '>', 'x', 'd', "+", 'p', "*", '1', '2', '3', '4']) +
        cycler('color', ['#1177b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#717f7f', '#bcbd22', '#17becf', 'k', 'k', 'k']))
    plt.rc('axes', prop_cycle=monochrome)

    # enable grid
    plt.grid(axis='y', alpha=0.7)

    return f

k = 2

w = 2

colors = {2:'#7a5195',
3: '#ef5675',
4: '#ffa600'}

diff_lenstra = {2:[], 3:[], 4:[]}
diff_2w = {2:[], 3:[], 4:[]}




fig = plt.figure()
ax = fig.add_subplot(111)

for w in [2,3,4]:

    x = []
    wary_data = []
    lenstra_data = []

    def wary(x):
        return 2**(k*w)-k+1 + math.floor((b-1)/w)*w + b*(1-1/(2**(k*w)))/w

    def lenstra(x):
        return 2**(k*w) - 2**(k*(w-1)) + b / (w+1/((2**k)-1)) + b-w

    for b in [2**k for k in range(14)]:
        x.append(b)
        wary_data.append(wary(b))
        lenstra_data.append(lenstra(b))
        diff_lenstra[w].append(2*b/lenstra(b))
        diff_2w[w].append(2*b/wary(b))

    plt.plot(x, wary_data, color=colors[w], marker='o', linestyle='dashed')
    plt.plot(x, lenstra_data, color=colors[w], marker = "o")

x = []
Separate = []
for b in [2**k for k in range(14)]:
    x.append(b)
    Separate.append(2*b)


plt.plot(x, Separate, color='#003f5c', marker='o')

plt.plot([], linestyle='dashed', color=colors[4], label='$2^w$-ary w=4')
plt.plot([], linestyle='dashed', color=colors[3], label='$2^w$-ary w=3')
plt.plot([], linestyle='dashed', color=colors[2], label='$2^w$-ary w=2')


plt.plot([], linestyle='solid', color=colors[4], label='YLL w=4')
plt.plot([], linestyle='solid', color=colors[3], label='YLL w=3')
plt.plot([], linestyle='solid', color=colors[2], label='YLL w=2')

plt.plot([], color='#003f5c', label="Separate")


#plt.title("Number of expected multiplications\nduring mutliexponentiation")
plt.xlabel("Exponent bitsize")
plt.ylabel("Expected number of multiplications")

plt.yscale('log')
plt.xscale('log')
plt.legend()

plt.grid(axis='y')

print(x)
ax.tick_params(axis=u'both', which=u'both',length=0)
plt.xticks(x, ['$2^{'+str(b)+'}$' for b in range(14)])


plt.savefig("theoretical_number_multiplications.pdf")

plt.show()


fig = plt.figure()
ax = fig.add_subplot(111)

for w in [2,3,4]:
    plt.plot(x, diff_lenstra[w], '-o' , color=colors[w])
    plt.plot(x, diff_2w[w], '-o', color=colors[w], linestyle = 'dashed')


plt.xscale('log')

axes = plt.gca()
xlim = axes.get_xlim()

plt.plot([], linestyle='dashed', color=colors[4], label='$2^w$-ary w=4')
plt.plot([], linestyle='dashed', color=colors[3], label='$2^w$-ary w=3')
plt.plot([], linestyle='dashed', color=colors[2], label='$2^w$-ary w=2')


plt.plot([], linestyle='solid', color=colors[4], label='YLL w=4')
plt.plot([], linestyle='solid', color=colors[3], label='YLL w=3')
plt.plot([], linestyle='solid', color=colors[2], label='YLL w=2')

plt.plot(xlim, [1,1], '', color='red', linestyle='dashdot', label="No speedup")




plt.xlabel("Exponent bitsize")
plt.ylabel("Speedup in percentage")
plt.legend()

#plt.title("Improvement of multiexponentiation algorithms\nover separate exponentiations")

ax.tick_params(axis=u'both', which=u'both',length=0)
plt.xticks(x, ['$2^{'+str(b)+'}$' for b in range(14)])
plt.grid(axis='y', alpha=0.7)
#plt.ylim([0,100])

plt.savefig("theoretical_improvement.pdf")
plt.show()
