import matplotlib.pyplot as plt
import math
import numpy as np


lmin = 7
lmax = 13
kmin = 2
kmax = 13



def process_file(l, k):
    file_name = "results/" + str(l) + "_" + str(k) + ".csv"

    f = open(file_name)
    mat = np.loadtxt(open(file_name, "r"), delimiter=";")
    return list(map(np.mean, list(mat.T)))

for l in [2**i for i in range(lmin, lmax, 2)]:
    speedup = []
    l_speedup = []

    for k in [2**i for i in range(kmin, kmax)]:
        data = process_file(l, k)
        speedup.append(data[1]/data[2])

        l_speedup.append(data[4]/data[6])




    plt.plot([2**i for i in range(kmin, kmax)], speedup, '-s', label="$\lambda = " + str(l) + "$ - S&M")
    plt.plot([2**i for i in range(kmin, kmax)], l_speedup, '--o', label="$\lambda = " + str(l) + "$ - Lenstra")

plt.xscale('log')
plt.legend()
plt.grid()

plt.ylabel("Size of the exponents $k$")
plt.ylabel("Computation time")

plt.title("Evolution of the Montgomery speed up\n over the size of the exponent for different values of $\lambda$")

plt.savefig("montgomery_sm.pdf")
plt.show()
