#ifndef W_ARRY_MONTGOMERY_H
#define W_ARRY_MONTGOMERY_H

#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>

/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */

void w_arry_montgomery_precomputations(BIGNUM** pc, int pow_w, BIGNUM* x, BIGNUM* y, BIGNUM* N, int w);

void w_arry_montgomery_exponentiation(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM* *pc);

#endif
