#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "lenstra.hpp"
#include "utils.hpp"

extern BN_CTX* ctx;

void lenstra_precomputations(BIGNUM** pc, int pow_w, BIGNUM* x, BIGNUM* y, BIGNUM* N)
{
        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        pc[i * pow_w + j] = BN_new();
                }
        }


        // Precomputation computations itself.
        BN_copy(pc[1 * pow_w + 0], x);
        BN_copy(pc[0 * pow_w + 1], y);
        BN_mod_mul(pc[2 * pow_w + 0], x, x, N, ctx);
        BN_mod_mul(pc[0 * pow_w + 2], y, y, N, ctx);

        int mid = pow_w/2;
        for(int i = 1; i < mid; i++)
        {
                BN_mod_mul(pc[(2*i+1) * pow_w + 0], pc[(2*(i-1)+1) * pow_w + 0], pc[2 * pow_w + 0], N, ctx);

                BN_mod_mul(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*(i-1)+1], pc[0 * pow_w + 2], N, ctx);
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < pow_w; j++)
                {
                        BN_mod_mul(pc[j * pow_w + 2*i+1], pc[(j-1) * pow_w + 2*i+1], x, N, ctx);
                }
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < mid; j++)
                {
                        BN_mod_mul(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j-1],y, N, ctx);
                }
        }
}


/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void lenstra_exponentiation(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM** pc)
{
        int pow_w = pow(2, w);

        int A = 0;
        int B = 0;

        BN_one(ret);
        int j = k-1;



        while(j>=0)
        {
                if(simultaneous_zero_bit(a, b, j))
                {
                        BN_mod_mul(ret, ret, ret, N, ctx);
                        j--;
                }
                else
                {
                        int j_new = std::max(j-w, -1);

                        int J = j_new+ 1;

                        while(simultaneous_zero_bit(a,b, J) == true)
                        {
                                J++;
                        }


                        A = filter(a, j, J);
                        B = filter(b, j, J);

                        while(j>=J)
                        {
                                BN_mod_mul(ret, ret, ret, N, ctx);
                                j--;
                        }

                        BN_mod_mul(ret, ret, pc[A * pow_w + B], N, ctx);


                        while(j>j_new)
                        {
                                BN_mod_mul(ret, ret, ret, N, ctx);
                                j--;
                        }

                }
        }
}
