#include "utils.hpp"
#include <cmath>

BIGNUM* trash = BN_new();

extern BN_CTX* ctx;
extern unsigned char* return_value;
extern int mode; // 0 : wary 
                 // 1 : lenstra

std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<long long, std::ratio<1, 1000000000>>> s;
std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<long long, std::ratio<1, 1000000000>>> r;

extern std::chrono::duration<double> overhead_lenstra[3];
extern std::chrono::duration<double> overhead_wary[3];

extern int count_lenstra[3];
extern int count_warry[3];


typedef struct bignum_st BIGNUM;

struct bignum_st
{
        BN_ULONG *d;   /* Pointer to an array of 'BN_BITS2' bit chunks. */
        int top;       /* Index of last used d +1. */
        /* The next are internal book keeping for bn_expand. */
        int dmax;      /* Size of the d array. */
        int neg;       /* one if the number is negative */
        int flags;
};


/**
 * Returns True if bits at index J of x and y are simultaneously 0, returns False otherwise
 * Used in the Lenstra multiexponentiation to alleviate the code reading.
 *
 * @param  x Exponent 1
 * @param  y Exponent 2
 * @param  j Index to test the bits
 * @return
 */
bool simultaneous_zero_bit(BIGNUM* x, BIGNUM* y, int j)
{
        int cj = j/BN_BITS2;

        BN_ULONG xx = *(x->d+cj);
        BN_ULONG yy = *(y->d+cj);


        int jj = j%BN_BITS2;

        if((xx>>jj)%2 == 0 && (yy>>jj)%2 == 0)
        {
                return true;
        }
        return false;

        /*

           if(!BN_is_bit_set(x, j) && !BN_is_bit_set(y, j))
           {
                return true;
           }
           else
           {
                return false;
           }

         */
}



int filter_openssl(BIGNUM* e, int i, int j)
{
        BN_rshift(trash, e, j);

        BN_mask_bits(trash, i-j+1);

        *return_value = 0;
        BN_bn2bin(trash, return_value);


        int return_final = (int)(*return_value);



        return return_final;
}


/**
 * [filter description]
 * @param  e [descriptison]
 * @param  j [description]
 * @param  J [description]
 * @return
 */

int filter(BIGNUM* e, int i, int j)
{
        int ci = i/BN_BITS2;
        int cj = j/BN_BITS2;

        int ii = i%BN_BITS2;
        int jj = j%BN_BITS2;

        int ret = 0;


        if(ci==cj)
        {
                BN_ULONG aa = *(e->d+ci);




                BN_ULONG b = aa >> jj;

                long c = (2<<(i-j));

                ret = b % c;

        }

        else
        {
                BN_ULONG al = *(e->d+ci);
                BN_ULONG ar = *(e->d+cj);

                //assert(ci == cj+1);

                if(ci == e->dmax)
                {
                        al = 0;
                }


                BN_ULONG right = ar >> jj;
                BN_ULONG left = al%(2<<(ii));

                ret = right + (left<<(BN_BITS2-jj));
        }

        return ret;
}


void BN_mod_mul_montgomery_custom(BIGNUM* res, BIGNUM* x, BIGNUM* y, BN_MONT_CTX* mctx, BN_CTX* ctx, int w){
        #ifdef OVERHEAD_MEASURE
        r = std::chrono::high_resolution_clock::now();
        #endif
        BN_mod_mul_montgomery(res, x, y, mctx, ctx);

        #ifdef OVERHEAD_MEASURE

        s = std::chrono::high_resolution_clock::now();
        if(mode == 0){overhead_wary[w-2] += s-r;count_warry[w-2]++;}
        else{overhead_lenstra[w-2] += s-r;count_lenstra[w-2]++;}
        #endif
}