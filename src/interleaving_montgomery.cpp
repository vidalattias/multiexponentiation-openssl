#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "interleaving_montgomery.hpp"
#include "utils.hpp"


extern BN_CTX* ctx;
extern BN_MONT_CTX* mctx;
extern BIGNUM* one_m;

void interleaving_precomputations_montgomery(BIGNUM** pc, int pow_w, BIGNUM* x_m, BIGNUM* y_m, BIGNUM* N)
{
        for(int i = 0; i<2; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        pc[i * (pow_w) + j] = BN_new();
                        //BN_copy(pc[i * (pow_w-1) + j], one_m);
                }
                BN_copy(pc[i * (pow_w)], one_m);
        }

        // Precomputation computations itself.
        BN_copy(pc[pow_w + 1], x_m);
        BN_copy(pc[1 * pow_w + 1], y_m);

        for(int j = 1; j < pow_w ; j++)
        {
          BN_mod_mul_montgomery(pc[0*(pow_w) + j], pc[0*(pow_w) + j-1], x_m, mctx, ctx);
          BN_mod_mul_montgomery(pc[1*(pow_w) + j], pc[1*(pow_w) + j-1], y_m, mctx, ctx);
        }
}


/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void interleaving_exponentiation_montgomery(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM** pc)
{
        int pow_w = pow(2, w);

        BIGNUM* E_x = BN_new();
        BIGNUM* E_y= BN_new();

        int interval_x = 0;
        int interval_y = 0;
        BN_copy(ret, one_m);

        int w_handle_x = -1;
        int w_handle_y = -1;

        int J = 0;

        for(int j = k-1; j >= 0; j--)
        {
          BN_mod_mul_montgomery(ret, ret, ret, mctx, ctx);

          if(w_handle_x == -1 && BN_is_bit_set(a, j) == 1)
          {
            J = j-w+1;

            while(BN_is_bit_set(a, J) == 0)
            {
              J = J+1;
            }
            w_handle_x = J;

            interval_x = filter(a, j, J);

            BN_copy(E_x, pc[0*(pow_w)+interval_x]);
          }

          if(w_handle_x == j)
          {
            BN_mod_mul_montgomery(ret, ret, E_x, mctx, ctx);
            w_handle_x = -1;
          }

        if(w_handle_y == -1 && BN_is_bit_set(b, j) == 1)
        {
          J = j-w+1;

          while(BN_is_bit_set(b, J) == 0)
          {
            J = J+1;
          }
          w_handle_y = J;
          interval_y = filter(b, j, J);

          BN_copy(E_y, pc[1*(pow_w)+interval_y]);
        }

        if(w_handle_y == j)
        {
          BN_mod_mul_montgomery(ret, ret, E_y, mctx, ctx);
          w_handle_y = -1;
        }
      }
}
