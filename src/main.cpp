#include <iostream>
#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <chrono>
#include <cmath>
#include <cassert>

#include "lenstra.hpp"
#include "lenstra_montgomery.hpp"
#include "w_arry_montgomery.hpp"
#include "utils.hpp"
#include "interleaving_montgomery.hpp"


int lambda, k;

BIGNUM*  x = BN_new();
BIGNUM*  y = BN_new();
BIGNUM*  x_m = BN_new();
BIGNUM*  y_m = BN_new();
BIGNUM*  N = BN_new();

BIGNUM* one_m = BN_new();


BN_CTX* ctx = BN_CTX_new();
BN_MONT_CTX* mctx = BN_MONT_CTX_new();
BIO *out;


int count_lenstra[3];
int count_warry[3];

unsigned char *return_value;

std::chrono::duration<double> overhead_lenstra[3];
std::chrono::duration<double> overhead_wary[3];


int mode = -1;

int main(int argc, char *argv[]) {

        lambda = std::atoi(argv[1]);
        k = std::atoi(argv[2]);
        int count = std::atoi(argv[3]);

        return_value = (unsigned char *) malloc(k);

        int w = 2;

        for(int i = 0; i<count; i++)
        {
                overhead_lenstra[0] = std::chrono::duration<double>::zero();
                overhead_lenstra[1] = std::chrono::duration<double>::zero();
                overhead_lenstra[2] = std::chrono::duration<double>::zero();

                overhead_wary[0] = std::chrono::duration<double>::zero();
                overhead_wary[1] = std::chrono::duration<double>::zero();
                overhead_wary[2] = std::chrono::duration<double>::zero();

                BIGNUM *p = BN_new();
                BIGNUM *q = BN_new();

                BN_generate_prime_ex(p, lambda/2, false, NULL, NULL, NULL);
                BN_generate_prime_ex(q, lambda/2, false, NULL, NULL, NULL);


                BN_mul(N, p, q, ctx);
                BN_MONT_CTX_set(mctx, N, ctx);



                BN_rand_range(x, N);
                BN_rand_range(y, N);



                BIGNUM*  a = BN_new();
                BIGNUM*  b = BN_new();

                BN_rand(a, k, BN_RAND_TOP_ONE, NULL);
                BN_rand(b, k, BN_RAND_TOP_ONE, NULL);








                BIGNUM *xx = BN_new();
                BIGNUM *yy = BN_new();
                BIGNUM *res = BN_new();


                std::chrono::duration<double> time2;
                auto start2 = std::chrono::high_resolution_clock::now();

                BN_mod_exp(xx, x, a, N, ctx);
                BN_mod_exp(yy, y, b, N, ctx);
                BN_mod_mul(res, xx, yy, N, ctx);

                auto end2 = std::chrono::high_resolution_clock::now();
                time2 = end2 - start2;




                std::cout << time2.count();





                for(int w = 2; w < 5; w++)
                {

                        int pow_w = pow(2, w);
                        BIGNUM* pc[pow_w][pow_w];
                        BIGNUM* pc_m[pow_w][pow_w];
                        BIGNUM* pc_w[pow_w][pow_w];

                        BIGNUM *result = BN_new();

                        auto start = std::chrono::high_resolution_clock::now();


                        lenstra_precomputations((BIGNUM **) pc, pow_w, x, y, N);
                        std::chrono::duration<double> time;

                        lenstra_exponentiation(result, x, y, a, b, N, k,w, (BIGNUM **) pc);

                        auto end = std::chrono::high_resolution_clock::now();
                        time = end - start;


                        // MONTGOMERY BEGIN
                        mode = 1;
                        std::chrono::duration<double> time_m;
                        auto start_m = std::chrono::high_resolution_clock::now();


                        BIGNUM *result_m = BN_new();
                        BIGNUM *result_m_final = BN_new();
                        BIGNUM* one = BN_new();
                        BN_one(one);
                        BN_to_montgomery(x_m, x, mctx, ctx);
                        BN_to_montgomery(y_m, y, mctx, ctx);
                        BN_to_montgomery(one_m, one, mctx, ctx);

                        lenstra_precomputations_montgomery((BIGNUM **) pc_m, pow_w, x_m, y_m, N, w);

                        lenstra_exponentiation_montgomery(result_m, x_m, y_m, a, b, N, k,w, (BIGNUM **) pc_m);

                        BN_from_montgomery(result_m_final, result_m, mctx, ctx);

                        auto end_m = std::chrono::high_resolution_clock::now();

                        time_m = end_m - start_m;
                        mode = -1;
                        // MONTGOMERY END





                        // W-ARRY BEGIN
                        mode = 0;
                        std::chrono::duration<double> time_w;
                        auto start_w = std::chrono::high_resolution_clock::now();


                        BIGNUM *result_w = BN_new();
                        BIGNUM *result_w_final = BN_new();
                        BN_to_montgomery(x_m, x, mctx, ctx);
                        BN_to_montgomery(y_m, y, mctx, ctx);
                        BN_to_montgomery(one_m, one, mctx, ctx);

                        w_arry_montgomery_precomputations((BIGNUM **) pc_w, pow_w, x_m, y_m, N, w);

                        w_arry_montgomery_exponentiation(result_w, x_m, y_m, a, b, N, k,w, (BIGNUM **) pc_w);

                        BN_from_montgomery(result_w_final, result_w, mctx, ctx);

                        auto end_w = std::chrono::high_resolution_clock::now();

                        time_w = end_w - start_w;
                        mode = -1;
                        // W-ARRY END


                        assert(BN_cmp(res, result) == 0);
                        assert(BN_cmp(res, result_m_final) == 0);
                        assert(BN_cmp(res, result_w_final) == 0);

                        std::cout << ";" << time.count() << ";" << time_m.count() << ";" <<time_w.count();
                }

                #ifdef OVERHEAD_MEASURE

                std::cout  << ";" << overhead_lenstra[0].count() << ";" << overhead_lenstra[1].count() << ";" << overhead_lenstra[2].count() << ";";
                
                std::cout << overhead_wary[0].count() << ";" << overhead_wary[1].count() << ";" << overhead_wary[2].count();

                #endif

                std::cout << std::endl;


                /*

                for(int w = 2; w <= 8; w++)
                {
                  int pow_w = pow(2, w);

                  BIGNUM* pc_i[2][pow_w];

                  // MONTGOMERY BEGIN
                  std::chrono::duration<double> time_i;
                  auto start_i = std::chrono::high_resolution_clock::now();


                  BIGNUM *result_i = BN_new();
                  BIGNUM *result_i_final = BN_new();
                  BIGNUM* one = BN_new();
                  BN_one(one);
                  BN_to_montgomery(x_m, x, mctx, ctx);
                  BN_to_montgomery(y_m, y, mctx, ctx);
                  BN_to_montgomery(one_m, one, mctx, ctx);



                  interleaving_precomputations_montgomery((BIGNUM **) pc_i, pow_w, x_m, y_m, N);

                  interleaving_exponentiation_montgomery(result_i, x_m, y_m, a, b, N, k,w, (BIGNUM **) pc_i);

                  BN_from_montgomery(result_i_final, result_i, mctx, ctx);

                  auto end_i = std::chrono::high_resolution_clock::now();

                  time_i = end_i - start_i;
                  // MONTGOMERY END

                  std::cout << time_i.count() << std::endl;

                  assert(BN_cmp(res, result_i_final) == 0);
                }

                   std::cout << BN_bn2dec(res) << std::endl;
                   std::cout << BN_bn2dec(result_m_final) << std::endl;
                   std::cout << BN_bn2dec(result_w_final) << std::endl;


                   std::cout << "Naive = " << BN_bn2dec(res) << std::endl;
                   std::cout << "Lenstra = " << BN_bn2dec(result) << std::endl;
                   std::cout << "Lenstra Montgo = " << BN_bn2dec(result_f) << std::endl;


                 */
        }
        return 0;
}
