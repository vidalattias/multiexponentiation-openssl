#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "lenstra.hpp"
#include "utils.hpp"

extern BN_CTX* ctx;
extern BN_MONT_CTX* mctx;
extern BIGNUM* one_m;

extern int count_warry[3];



void w_arry_montgomery_precomputations(BIGNUM** pc, int pow_w, BIGNUM* x, BIGNUM* y, BIGNUM* N, int w)
{
        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {

                        pc[i * pow_w + j] = BN_new();
                        BN_copy(pc[i * pow_w + j], one_m);
                }
        }


        // Precomputation computations itself.
        for(int i = 0; i < pow_w; i++)
        {
                int j;
                if(i==0)
                {
                        BN_copy(pc[0], one_m);
                        BN_copy(pc[1], y);
                        j = 2;
                }
                else
                {
                        j = 1;
                        if(i == 1)
                        {
                                BN_copy(pc[pow_w], x);
                        }
                        else
                        {
                                BN_mod_mul_montgomery_custom(pc[i*pow_w], pc[(i-1)*pow_w], x, mctx, ctx, w);
                        }
                }

                for(; j<pow_w; j++)
                {
                        BN_mod_mul_montgomery_custom(pc[i * pow_w + j], pc[i * pow_w + j - 1], y, mctx, ctx, w);
                }
        }


        /*

           for(int i = 0; i<pow_w; i++)
           {
                for(int j = 0; j < pow_w; j++)
                {
                        std::cout << BN_bn2dec(pc[j*pow_w+i]) << "\t";
                }
                std::cout << std::endl;
           }

         */
}

void w_arry_montgomery_exponentiation(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM* *pc)
{
        int pow_w = pow(2, w);
        BN_copy(ret, one_m);

        for(int j = floor((k-1)/w)*w; j >= 0; j-=w)
        {
                for(int n = 1; n <= w; n++)
                {
                        BN_mod_mul_montgomery_custom(ret, ret, ret, mctx, ctx, w);
                }
                int A = filter(a, j+w-1, j);
                int B = filter(b, j+w-1, j);
                if(A != 0 || B != 0)
                {
                        BN_mod_mul_montgomery_custom(ret, ret, pc[A*pow_w + B], mctx, ctx, w);
                }
        }
}
